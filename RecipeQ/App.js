import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Final from './Final'
import firebase from "firebase/app";

const firebaseConfig = {
  apiKey: "AIzaSyCQD76HIiwUU1jYDQt6QEvXjzbfuKSFOeM",
  authDomain: "recipeq-3e438.firebaseapp.com",
  projectId: "recipeq-3e438",
  storageBucket: "recipeq-3e438.appspot.com",
  messagingSenderId: "98251789197",
  appId: "1:98251789197:web:ad0f2cc26e2720e2ea23c4"
};

if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig);
}

export default function App() {
  return (
  <Final/>
  );
}

