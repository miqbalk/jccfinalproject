import React from "react";
import { StyleSheet, Text, View, Image, TextInput, Button, TouchableOpacity, } from "react-native";
import { MaterialIcons, MaterialCommunityIcons,  Ionicons, Zocial } from '@expo/vector-icons';
export default function About(){
    return (
        <View style={styles.container}>
            <View style={styles.userDetails}>
                <Text style={styles.textTitle}>Profil Saya</Text>
                <Text style={styles.textTitle}>React Native Developer</Text>
            </View>
            <View style={styles.subContent}>
                <View style={styles.subContentTitle}>
                    <Text style={styles.textTitle}>Portofolio</Text>
                </View>
                <View style={styles.listContent}>
                    <View style={styles.content}>
                        <MaterialCommunityIcons name="gitlab" size={40} color="black" />
                        <Text style={styles.textContent}>@miqbalk</Text>
                    </View>
                    <View style={styles.content}>
                        <Ionicons name="logo-github" size={40} color="black" />
                        <Text style={styles.textContent}>@miqbalk</Text>
                    </View>
                </View>
            </View>
            <View style={styles.subContent}>
                <View style={styles.subContentTitle}>
                    <Text style={styles.textTitle}>Contact</Text>
                </View>
                <View style={styles.listContent}>
                    <View style={styles.content}>
                    <Ionicons name="logo-linkedin" size={40} color="steelblue" />
                    <Text style={styles.textContent}>@miqbalk</Text>
                    </View>
                    <View style={styles.content}>
                        <Ionicons name="logo-whatsapp" size={40} color="green" />
                        <Text style={styles.textContent}>081299185025</Text>
                    </View>
                    <View style={styles.content}>
                        <Zocial name="gmail" size={40} color="red" />
                        <Text style={styles.textContent}>miqbalk556@gmail.com</Text>
                    </View>
                </View>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        flexDirection:'column',
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
        padding:20
      },

    userDetails:{
        flex:5,
        marginTop:60,
        flexDirection:'column',
        alignItems:'center'
    },

    textTitle:{
        fontSize:25,
        fontWeight:'bold'
    },

    subContent:{
        flex:2.5,
        flexDirection:'column',
        marginTop:20,
        backgroundColor:'#EFEFEF',
        padding:10,
        paddingRight:30,
        paddingBottom:30,
        width:'100%',
    },

    subContentTitle:{
        borderBottomWidth:1
    },

    listContent:{
        flexDirection:'row',
        justifyContent:'space-evenly'
    },

    content:{
        flexDirection:'column',
        justifyContent:'center',
        alignItems:'center'
    },

    textContent:{
        fontSize:10
    }

})