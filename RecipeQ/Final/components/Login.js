import React, { useState } from "react";
import { Image, StyleSheet, Text, View, TextInput, Button,TouchableOpacity, StatusBar, ActivityIndicator } from "react-native";
import { useFonts } from 'expo-font';
import {MaterialIcons} from '@expo/vector-icons';
import { SafeAreaView } from "react-native-safe-area-context";
import firebase from "firebase";

export default function Login({ navigation }) {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [isError, setIsError] = useState(false);

  const submit = () => {
    
      firebase.auth().signInWithEmailAndPassword(email, password)
      .then((userCredential) => {
        var user = userCredential.user.providerData[0];
        console.log(user.email);
        setEmail("")
        setPassword("")
        alert("Login Success !");
        navigation.navigate("MainMenu",{
          screen: 'Home',
          params: {
            email : user.email
          },
        })
      })
      .catch((error) => {
        console.log(error.code, error.message);
        alert(error.message)
      });
    };

    return (
      <View style={styles.container}>
          <View style={styles.banner}>
          <MaterialIcons name="fastfood" size={60} color="white" />
          <Text style={{ fontSize: 30, fontWeight: "bold",color:"white" }}>RecipeQ</Text>
          </View>
          <View style={styles.containerLogin}>
          
              <Text style={{fontWeight:'bold',fontSize:25}}>Login</Text>
            <TextInput
              style={{
                borderBottomWidth:1,
                borderBottomColor:"#DDDDDD",
                paddingVertical: 10,
                borderRadius: 5,
                width: "85%",
                marginBottom: 10,
                paddingHorizontal: 10,
              }}
              placeholder="Masukan Username"
              value={email}
              onChangeText={(value)=>setEmail(value)}
            />
            <TextInput
              style={{
                borderBottomWidth:1,
                borderBottomColor:"#DDDDDD",
                paddingVertical: 10,
                borderRadius: 5,
                width: "85%",
                marginBottom: 10,
                paddingHorizontal: 10,
              }}
              placeholder="Masukan Password"
              value={password}
              secureTextEntry={true}
              onChangeText={(value)=>setPassword(value)}
            />
             <TouchableOpacity
              style={styles.buttonLogin}
              onPress={submit}
              >
                <Text style={{color:'white'}}>Login</Text>
             </TouchableOpacity>
             <TouchableOpacity
              style={styles.buttonRegister}
              onPress={() => navigation.navigate("Register")}
              >
                <Text style={{color:'#61DEA4'}}>Register</Text>
             </TouchableOpacity>
          </View>
      </View>
  );
}  

const styles = StyleSheet.create({
  container: {
    marginTop:StatusBar.currentHeight,
    flex: 1,
    backgroundColor: "white",
    justifyContent: "center",
    alignItems: "center",
  },

  banner:{
      flex:1,
      backgroundColor: "#61DEA4",
      justifyContent: "center",
      alignItems: "center",
      width:"100%"
  },

  containerLogin:{
    flex: 3,
    justifyContent: "center",
    alignItems: "center",
    width:"100%"
  },
  
  buttonLogin:{
    marginTop:10,
    alignItems: "center",
    backgroundColor: "#61DEA4",
    padding: 12,
    width:"40%",
    borderRadius:20
  },

  buttonRegister:{
    borderWidth:1,
    borderColor:"#61DEA4",
    marginTop:10,
    alignItems: "center",
    backgroundColor: "white",
    padding: 12,
    width:"40%",
    borderRadius:20
  }
});
