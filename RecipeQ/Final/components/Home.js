import React, { useEffect } from 'react'
import { useState } from 'react'
import { StyleSheet, Text, View, Image, Button, FlatList, ActivityIndicator, TextInput, TouchableOpacity,Dimensions} from 'react-native'
import { SafeAreaProvider, SafeAreaView } from 'react-native-safe-area-context';
import axios from 'axios';
import Login from './Login';
import { MaterialIcons, MaterialCommunityIcons,  Ionicons, Zocial } from '@expo/vector-icons';
import { NavigationHelpersContext } from '@react-navigation/core';


export default function Home({navigation, route}) {
    const { email } = route.params
    const [isLoading,setLoading] = useState(false);
    const [items, setItem] = useState([]); 
    const [keyword, setKeyword] = useState('');

    function getData(){
       axios.get(`https://masak-apa-tomorisakura.vercel.app/api/recipes`)
        .then(res=>{
            const data1 = (res.data.results)
            console.log(keyword);
            setItem(data1);
            setLoading(false);
        })
        .catch(err=>{
            console.log('error: ', err);
        })
    }

    function getSearch(){
        axios.get(`https://masak-apa-tomorisakura.vercel.app/api/search/?q=${keyword}`)
         .then(res=>{
             const data1 = (res.data.results)
             // console.log('res: ', data1);
             console.log(keyword);
             let total = data1.length
             setItem(data1);
             alert(`${total} Hasil Ditemukan ! untuk "${keyword}"`);
             setKeyword('')
             setLoading(false);

         })
         .catch(err=>{
             console.log('error: ', err);
         })
     }

    const onPress = ()=>{
        setLoading(true);
        getSearch();
    }
       
    useEffect(() => {
        setLoading(true);
        getData();
    },[])

    if(isLoading){
        return (
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
            <ActivityIndicator size="large" color="#61DEA4" />
        </View>
        );
    } else {
        return (
        
            <View style={styles.container}>   
                <SafeAreaView style={{paddingVertical:5, paddingHorizontal:15,width:"100%"}}>
                    <View style={{flexDirection:'row',alignItems:'center',justifyContent:'flex-end'}}>
                        <Text>Selamat Datang, </Text>
                        <Text style={{fontSize:18, fontWeight:'bold',marginLeft:20}}>{email}</Text>
                    </View>
                    <View style={{flexDirection:'row',justifyContent:'center',alignItems:'center'}}>
                        <View
                            style={{
                            backgroundColor: '#fff',
                            padding: 8,
                            marginVertical: 10,
                            borderRadius: 10,
                            borderWidth:1,
                            flexDirection:'row',
                            width:"85%"
                            }}
                        >
                            <TextInput
                            clearButtonMode="always"
                            autoCapitalize="none"
                            autoCorrect={false}
                            placeholder="Search"
                            value={keyword}
                            onChangeText={(value) => setKeyword(value)}
                            style={{ backgroundColor: '#fff', paddingHorizontal: 10}}
                            />
                        </View>
                        <TouchableOpacity
                                style={{
                                    padding: 6,
                                    marginVertical: 10,
                                    marginLeft:5,
                                    borderRadius: 5,
                                    width:"15%",
                                    backgroundColor:"#61DEA4",
                                    alignItems:'center',  
                                }}
                                onPress={onPress}
                        >
                         <MaterialIcons name="search" size={32} color="white" />
                        </TouchableOpacity>
                    </View>
                </SafeAreaView>
                <FlatList 
                        columnWrapperStyle={{
                            flex:1,
                            justifyContent:'space-around'
                        }}
                        data={items}
                        numColumns={2}
                        keyExtractor={(item) => item.key}
                        renderItem={({item})=>{
                            return (
                                    <View style={styles.listItem}>
                                            <Image 
                                                source={{uri: item.thumb }} 
                                                resizeMode="cover" 
                                                style={styles.image}
                                            />
                                            <View style={styles.underLine}></View>
                                            <Text style={styles.text}>{item.title.substring(0,35)}...</Text>
                                            <TouchableOpacity
                                                style={styles.btnDetail}
                                                onPress={()=>{
                                                    navigation.navigate('Detail',{
                                                        key : item.key,
                                                        thumb : item.thumb,
                                                    })
                                                }}
                                            >
                                                <Text style={{color:'white'}}>Lihat Resep</Text>
                                            </TouchableOpacity>
                                    </View>
                            )   
                        }} 
                        />
            </View>
        
            
        )
    }
}

const styles = StyleSheet.create({
    container:{
        flex:1,        
        backgroundColor:'#f0f0f0', 
        justifyContent:'center',
        alignItems:'center'
        
    },         

    listItem:{
        paddingVertical: 10,
        paddingHorizontal: 10,
        marginBottom:10,
        backgroundColor: '#fff',
        maxWidth: Dimensions.get('window').width /2,
        width:"48%",
        backgroundColor: '#fff',
        borderRadius: 4,
    },

    image: {
        width:"100%",
        height:120,
        borderRadius:10,
        marginBottom:5
    },
    underLine:{
        width:"100%",
        backgroundColor:"#61DEA4",
        height:2
    },

    text: {
        color:"black",
        fontSize:12,
        fontWeight: "bold",
        textAlign: "center",
      },

    btnDetail:{
        marginTop:5,
        alignItems: "center",
        backgroundColor: "#61DEA4",
        padding: 5,
        width:"100%",
        borderRadius:3,
        position:'relative'
        
    }
})
