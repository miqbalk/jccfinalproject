import React, {useEffect,useState} from 'react'
import { StyleSheet, Text, View ,ActivityIndicator,Image} from 'react-native'
import axios from 'axios';
import { SafeAreaView, useSafeAreaFrame } from 'react-native-safe-area-context';
import { ScrollView } from 'react-native-gesture-handler';

export default function Detail({navigation,route}) {
    const {key, thumb} = route.params; 
    const [isLoading,setLoading] = useState(false);
    const [item,setItems] = useState([]);

    function getDetail(){
        axios.get(`https://masak-apa-tomorisakura.vercel.app/api/recipe/${key}`)
         .then(res=>{
             const data1 = (res.data.results)
            //  console.log('res: ', data1);
            //  console.log(key,thumb);
             setItems(data1);
             setLoading(false);
         })
         .catch(err=>{ 
             console.log('error: ', err);
         })
     }
     
     useEffect(() => {
         setLoading(true);
         getDetail();
     },[])

     
     if(isLoading){
        return (
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
            <ActivityIndicator size="large" color="#61DEA4" />
        </View>
        );
    } else {
            return (
                <View style={styles.container}>
                    <View 
                        style={{
                            paddingHorizontal:8,
                            paddingVertical:5,
                            backgroundColor:'white',
                            borderRadius:10,
                            borderColor:"#61DEA4",
                            borderWidth:2,
                            marginBottom:5
                        }}
                    >
                        <Text style={styles.title}>{item.title}</Text>
                    {/* <View style={styles.underLine}></View> */}
                    </View>
                    <Image 
                        source={{uri: thumb}} 
                        resizeMode="cover" 
                        style={styles.image}
                    />
                    <ScrollView style={{
                        
                    }}>
                        <Text>{item.step}</Text>
                    </ScrollView>
                </View>
            );
        
    }
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        justifyContent: 'flex-start',
        alignItems:'center',
        margin:10
    },

    title:{
        textAlign:'center',
        fontWeight: "bold",
        fontSize:18,
        color:"#333333"
    },

    image: {
        width:"100%",
        height:180,
        borderRadius:10,
        marginBottom:5
    },

    underLine:{
        width:"100%",
        backgroundColor:"#61DEA4",
        height:3
    },
})
