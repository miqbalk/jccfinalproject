import React, { useState } from "react";
import { Image, StyleSheet, Text, View, TextInput, Button,TouchableOpacity, StatusBar, ActivityIndicator } from "react-native";
import {MaterialIcons} from '@expo/vector-icons';
import { SafeAreaView } from "react-native-safe-area-context";
import firebase from "firebase";

export default function Login({ navigation }) {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [passConfirm, setPassConfirm] = useState("");
  const [isError, setIsError] = useState(false);

  const submit = () => {

    if(password === passConfirm){
        firebase.auth().createUserWithEmailAndPassword(email, password)
        .then((userCredential) => {
        var user = userCredential.user;
        console.log(user);
        setEmail("");
        setPassword("");
        setPassConfirm("");
        alert("Registration Success !")
        })
        .catch((error) => {
        var errorCode = error.code;
        var errorMessage = error.message;
        console.log(error.code, error.message);
        alert(error.message);
        });
    }else{
        alert("Password dan Konfirmasi Password Tidak Sama !")
    }
    };

    return (
      <View style={styles.container}>
          <View style={styles.banner}>
          <MaterialIcons name="fastfood" size={60} color="white" />
          <Text style={{ fontSize: 30, fontWeight: "bold",color:"white" }}>RecipeQ</Text>
          </View>
          <View style={styles.containerLogin}>
              <Text style={{fontWeight:'bold',fontSize:25}}>Register</Text>
            <TextInput
              style={{
                borderBottomWidth:1,
                borderBottomColor:"#DDDDDD",
                paddingVertical: 10,
                borderRadius: 5,
                width: "85%",
                marginBottom: 10,
                paddingHorizontal: 10,
              }}
              placeholder="Masukan Email"
              value={email}
              onChangeText={(value)=>setEmail(value)}
            />
            <TextInput
              style={{
                borderBottomWidth:1,
                borderBottomColor:"#DDDDDD",
                paddingVertical: 10,
                borderRadius: 5,
                width: "85%",
                marginBottom: 10,
                paddingHorizontal: 10,
              }}
              placeholder="Masukan Password"
              value={password}
              secureTextEntry={true}
              onChangeText={(value)=>setPassword(value)}
            />
            <TextInput
              style={{
                borderBottomWidth:1,
                borderBottomColor:"#DDDDDD",
                paddingVertical: 10,
                borderRadius: 5,
                width: "85%",
                marginBottom: 10,
                paddingHorizontal: 10,
              }}
              placeholder="Konfirmasi Password"
              value={passConfirm}
              secureTextEntry={true}
              onChangeText={(value)=>setPassConfirm(value)}
            />
             <TouchableOpacity
              style={styles.buttonLogin}
              onPress={submit}
              >
                <Text style={{color:'white'}}>Register</Text>
             </TouchableOpacity>
             <TouchableOpacity
              style={styles.buttonRegister}
              onPress={() => navigation.navigate("Login")}
              >
                <Text style={{color:'#61DEA4'}}>Login</Text>
             </TouchableOpacity>
          </View>
      </View>
  );
}  
const styles = StyleSheet.create({
  container: {
    marginTop:StatusBar.currentHeight,
    flex: 1,
    backgroundColor: "white",
    justifyContent: "center",
    alignItems: "center",
  },

  banner:{
      flex:1,
      backgroundColor: "#61DEA4",
      justifyContent: "center",
      alignItems: "center",
      width:"100%"
  },

  containerLogin:{
    flex: 3,
    justifyContent: "center",
    alignItems: "center",
    width:"100%"
  },
  
  buttonLogin:{
    marginTop:10,
    alignItems: "center",
    backgroundColor: "#61DEA4",
    padding: 12,
    width:"40%",
    borderRadius:20
  },

  buttonRegister:{
    borderWidth:1,
    borderColor:"#61DEA4",
    marginTop:10,
    alignItems: "center",
    backgroundColor: "white",
    padding: 12,
    width:"40%",
    borderRadius:20
  }
});
