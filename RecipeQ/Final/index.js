import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { MaterialIcons, MaterialCommunityIcons,  Ionicons, Zocial } from '@expo/vector-icons';


import Login from './components/Login';
import Home from './components/Home';
import About from './components/About';
import Register from './components/Register';
import Detail from './components/Detail';

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

export default function index() {
    return (
       <NavigationContainer>
          <Stack.Navigator>
              <Stack.Screen name='Login' component={Login} options={{ headerShown: false }} />
              <Stack.Screen name='Register' component={Register} options={{ headerShown: false }}/>
              <Stack.Screen name='MainMenu' component={MainMenu} options={{ headerShown: false }}/>
              <Stack.Screen name='Detail' component={Detail} />
          </Stack.Navigator>
       </NavigationContainer>
    )
}

const MainMenu = ()=>(
  <Tab.Navigator 
    screenOptions={
      {headerShown:false}
    }
    tabBarOptions={{
      activeTintColor: 'green',
      inactiveTintColor: 'gray',
    }}
  >
    <Tab.Screen name="Home" component={Home} options={{
      tabBarIcon : (tabInfo) => {
        return (
          <MaterialIcons name="home" size={32}  color={tabInfo.focused ? "green" : "grey"} />
        )
      }
    }}/>
    <Tab.Screen name="About" component={About} options={{
      tabBarIcon : (tabInfo) => {
        return (
          <MaterialIcons name="account-box" size={32}  color={tabInfo.focused ? "green" : "grey"} />
        )
      }
    }}/>
  </Tab.Navigator>        
)